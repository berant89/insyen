/*
Benjamin Eran-Tasker 260377984
Dominik Laskowski 260374752
*/

#include "stm32f4xx.h"
#include "stm32f4xx_adc.h"
#include "arm_math.h"
#include <stdio.h>
#include "main.h"

#define TIME_DELAY 125 //125ms LED delay

#define BUFFER_SIZE 100

//Global variable
static __IO uint32_t TimingDelay; //Amount of time remaining
float trend, prev_temp; //Trend is the previous trend, prev_temp is the previous temperature recorded.

//The common ADC struct that sets the common behavious for all the ADCs
ADC_CommonInitTypeDef common = { ADC_Mode_Independent, ADC_Prescaler_Div2, ADC_DMAAccessMode_Disabled, ADC_TwoSamplingDelay_5Cycles };
//The ADC struct the defines its specific functions
ADC_InitTypeDef ad = {ADC_Resolution_12b, DISABLE, ENABLE, ADC_ExternalTrigConvEdge_None, ADC_ExternalTrigConv_T1_CC1, ADC_DataAlign_Right, 1};
//The GPIO struct that defines the pins, the mode, speed and how it reads bits.
GPIO_InitTypeDef gp = {GPIO_Pin_0, GPIO_Mode_AN, GPIO_Speed_50MHz, GPIO_OType_PP, GPIO_PuPd_UP};

/**
* Kalman state struct.
*/
typedef struct
{
	float q;
	float r;
	float x;
	float p;
	float k;
}kalman_state;

/**
* Updates the state of the Kalman Filter, using the assembly subroutine.
*	@param kf The Kalman Filter struct.
* @param meas The current measurement.
* @return 0 if successful or 1 if there's an error.
*/
int kalman(kalman_state* restrict kf, float meas);


/**
  * @brief  Inserts a delay time.
  * @param  nTime: specifies the delay time length, in milliseconds.
  * @retval None
  */
void Delay(__IO uint32_t nTime)
{ 
  TimingDelay = nTime;

  while(TimingDelay != 0);
}

/**
  * @brief  Decrements the TimingDelay variable.
  * @param  None
  * @retval None
  */
void TimingDelay_Decrement(void)
{
  if (TimingDelay != 0x00)
  { 
    TimingDelay--;
  }
}
/**
* Sets ups and configures the peripheral clock, initializes the ADC, GPIO, the user button and the LEDS.
*/
void setup()
{
	//Enables the high speed APB (APB2) peripheral clock.
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);
	//Enables the AHB1 peripheral clock.
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	//Initializes the GPIO with the peripheral GPIOA
	GPIO_Init(GPIOA, &gp);
	
	//Initializes the common ADC behaviours
	ADC_CommonInit(&common);
	
	//Enables the temperature sensor
	ADC_TempSensorVrefintCmd(ENABLE);
	
	//Initializes ADC1
	ADC_Init(ADC1, &ad);
	ADC_Cmd(ADC1, ENABLE);
	
	//Sets up user button
	STM_EVAL_PBInit(BUTTON_USER, BUTTON_MODE_GPIO);
	
	//Initialize LEDs mounted on board
	STM_EVAL_LEDInit(LED4);
  STM_EVAL_LEDInit(LED3);
  STM_EVAL_LEDInit(LED5);
  STM_EVAL_LEDInit(LED6);
}

/**
* Reads the 12 bit value in the given channel of ADC1.
* @param channel One of the 16 channels available to the ADCs.
* @return Returns a 12 bit value representing the voltage.
*/
uint16_t readADC1(uint8_t channel)
{
	//Configures for the selected ADC regular channel
	ADC_RegularChannelConfig(ADC1, channel, 1, ADC_SampleTime_144Cycles);
	//Start the conversion
	ADC_SoftwareStartConv(ADC1);
	// Wait until conversion completion 
	while(ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET);
	//Get the conversion value
	return ADC_GetConversionValue(ADC1);
}

/**
* Using the given temperature it turns on the appropriate LED
* then it rotates the LEDs either clockwise or counter clockwise
* while keeping the appropriate LED on.
* @param temp The temperature of the chip.
*/
void light_spin(float temp)
{
	/*The difference between the current temperature and the previous temperature.
	This is used to compare the past trend and the current trend.*/
	register float tmp = temp - prev_temp;
	//The order of LEDs to be lit on the board
	register Led_TypeDef l1, l2, l3;
	
	/*Check if the recorded temperature falls between the bounds and turn
	on the appropriate LED and assign the other LEDs that will rotate.
	Note:1)
				LED3 = Orange
				LED4 = Green
				LED5 = Red
				LED6 = Blue
			 2) Orientation
						LED3
				 LED4		LED5
						LED6*/
	if(temp < 30)
	{
		//Turn on the LED
		STM_EVAL_LEDOn(LED4);
		l1 = LED3;
		l2 = LED5;
		l3 = LED6;
	}
	else
		if(temp >= 30 && temp <= 40)
		{
			STM_EVAL_LEDOn(LED3);
			l1 = LED5;
			l2 = LED6;
			l3 = LED4;
		}
		else
			if(temp > 40)
			{
				STM_EVAL_LEDOn(LED5);
				l1 = LED6;
				l2 = LED4;
				l3 = LED3;
			}
	
	/*Clockwise rotation if the old trend is greater than the current trend.
		This represents that the temperature decreased.
	*/
	if(trend > tmp)
	{
		STM_EVAL_LEDToggle(l1); //Turn on LED
		Delay(TIME_DELAY); //Wait for some time in ms
		STM_EVAL_LEDToggle(l1); //Turn off the previous LED
		STM_EVAL_LEDToggle(l2); //Turn on LED
		Delay(TIME_DELAY);
		STM_EVAL_LEDToggle(l2);//Turn off the previous LED
		STM_EVAL_LEDToggle(l3);
		Delay(TIME_DELAY);
		STM_EVAL_LEDToggle(l3);
	}
	else
	{
		/*Counterclockwise rotation if the old trend is smaller than the current trend.
		This represents that the temperature increased.
		*/
		if(trend < tmp)
		{
			STM_EVAL_LEDToggle(l3); //Turn on LED
			Delay(TIME_DELAY); //Wait for some time in ms
			STM_EVAL_LEDToggle(l3); //Turn off the previous LED
			STM_EVAL_LEDToggle(l2); //Turn on LED
			Delay(TIME_DELAY);
			STM_EVAL_LEDToggle(l2); //Turn off the previous LED
			STM_EVAL_LEDToggle(l1);
			Delay(TIME_DELAY);
			STM_EVAL_LEDToggle(l1);
		}
	}
	//If it statisfies neither of those conditions then it means there was no change
	//and the corresponding temp LED stays on with no rotations.
	
	//Update the trend
	trend = tmp;
}

/*
* Turns off the 4 LEDs on the board
*/
void light_off()
{
	STM_EVAL_LEDOff(LED3);
	STM_EVAL_LEDOff(LED4);
	STM_EVAL_LEDOff(LED5);
	STM_EVAL_LEDOff(LED6);
}

int main()
{
	/*
	Vs is the variable that will contain the recorded voltage in mV.
	Vref is the voltage at 25'C in mV.
	*/
	float Vs, Vref = 760;
	//Temperature
	float temp;
	
	//The buffer.
	float buffer[BUFFER_SIZE];
	//The index of the buffer.
	unsigned int current = 0;
	
	//Kalman state
	kalman_state kf;
	kf.q = 0.5; //Process covariance
	kf.r = 8; // Measurement covariance
	kf.p = 0;
	kf.x = 0;
	kf.k = 0;
	
	//Set the initial trend and previous temperature at 0.
	trend = 0, prev_temp = 0;
	
	/*Start configuring ADC1, ADC common behaviours and GPIO.
	Enable the temperature sensor and the LEDs*/
	setup();
	
	/* Setup Systick Timer for 1 msc interrupts.
	SystemCoreClock = 1KHz
	Modifications were done inside stm32f4xx_it.c to handle the timingDelay decrement,
	go to line 142 to view the function void SysTick_Handler(void).
	*/
	if (SysTick_Config(SystemCoreClock / 1000))
  { 
		printf("SysTick error\n");
    while (1);
  }
	
	//This loop waits until the button is pressed.
  while (!STM_EVAL_PBGetState(BUTTON_USER))
		Delay(100);
	
	//This waits until the button is released to start the program.
	while (STM_EVAL_PBGetState(BUTTON_USER))
		Delay(100);
	
	//The above two loops are needed as to avoid prematurely terminating the program.
	
	printf("Start\n");
	
	/*
	Keep looping until the user push the button to stop the program
	*/
	while (!STM_EVAL_PBGetState(BUTTON_USER))
	{
			//Obtain the 12 bit value then convert it in millivolts between 0 and 3000.
			Vs = readADC1(ADC_Channel_TempSensor) * 3000 / 0xfff;
			//Calculuate the current temperature.
			temp = (Vs - Vref) / 2.5f + 25.0f;
			//Filter the temperature to remove any noise.
			kalman(&kf, temp);
			//Store the value in the buffer.
			buffer[current] = kf.x;
			//Turn off LEDs for new temperature
			light_off();
			//Send the current temperature and spin the LEDs.
			light_spin(kf.x);
			printf("%f\n", kf.x);
			//Update the index and wrap around.
			current = (current + 1) % BUFFER_SIZE;
  }
	
	printf("Stop\n");
	return 0;
}
