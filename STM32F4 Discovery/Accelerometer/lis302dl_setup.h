#ifndef LIS302DL_SETUP_H
#define LIS302DL_SETUP_H
//Includes
#include "stm32f4_discovery.h"
#include "stm32f4_discovery_lis302dl.h"
#include "math.h"

//Exported types
//Exported constants
#define DOUBLECLICK_Z                    ((uint8_t)0x60)
#define SINGLECLICK_Z                    ((uint8_t)0x50)
#define NUM_MEASUREMENTS 10 //Number of measurements to take for calibration.
//Exported macro
//Exported functions

uint32_t LIS302DL_TIMEOUT_UserCallback(void);
void wait_for_button_press(void);
void read_average_accel(float*, float*, float*);
void calibrate(void);
int setup_lis302dl(void);

#endif
