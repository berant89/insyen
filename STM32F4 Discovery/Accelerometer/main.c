#include "main.h"

#define TIME_DELAY 125 //125ms LED delay

__IO uint32_t TimingDelay = 0;

__IO uint8_t SingleClickDetect = 0x00;
extern uint8_t ClickReg;

/**
  * @brief  Inserts a delay time.
  * @param  nTime: specifies the delay time length, in milliseconds.
  * @retval None
  */
void Delay(__IO uint32_t nTime)
{ 
  TimingDelay = nTime;

  while(TimingDelay != 0);
}

/**
  * @brief  Decrements the TimingDelay variable.
  * @param  None
  * @retval None
  */
void TimingDelay_Decrement(void)
{
  if (TimingDelay != 0x00)
  { 
    TimingDelay--;
  }
}


/**
  * @brief  Setups both LIS302DL and TIM4 and calibrates LIS302DL and runs the main program.
  * @param  None.
  * @retval None.
  */
int main()
{
    //Setup accelerometer
	setup_lis302dl();
    //Setup TIM4
	TIM_Config();
	// Initialize user button
	STM_EVAL_PBInit(BUTTON_USER, BUTTON_MODE_GPIO);
	//Calibrate the accelerometer
	calibrate();
	
	while (1)
  {
    if (SingleClickDetect != 0)
    {
      Delay(50);
      /* Read click status register */
      LIS302DL_Read(&ClickReg, LIS302DL_CLICK_SRC_REG_ADDR, 1);
      
      if (ClickReg == DOUBLECLICK_Z)
      {
        /* Enable TIM4 Capture Compare Channel 1 */
        TIM_CCxCmd(TIM4, TIM_Channel_1, ENABLE);
        /* Sets the TIM4 Capture Compare1 Register value */
        TIM_SetCompare1(TIM4, TIM_CCR/12);
        /* Enable TIM4 Capture Compare Channel 2 */
        TIM_CCxCmd(TIM4, TIM_Channel_2, ENABLE);
        /* Sets the TIM4 Capture Compare2 Register value */
        TIM_SetCompare2(TIM4, TIM_CCR/12);
        /* Enable TIM4 Capture Compare Channel 3 */
        TIM_CCxCmd(TIM4, TIM_Channel_3, ENABLE);
        /* Sets the TIM4 Capture Compare3 Register value */
        TIM_SetCompare3(TIM4, TIM_CCR/12);
        /* Enable TIM4 Capture Compare Channel 4 */
        TIM_CCxCmd(TIM4, TIM_Channel_4, ENABLE);
        /* Sets the TIM4 Capture Compare4 Register value */
        TIM_SetCompare4(TIM4, TIM_CCR/12);
        
        /* Time base configuration */
        TIM_SetAutoreload(TIM4, TIM_ARR/12);
        Delay(200);
        
        /* Clear the click status register by reading it */
        LIS302DL_Read(&ClickReg, LIS302DL_CLICK_SRC_REG_ADDR, 1);
        
        /* Reset the single click detect */
        SingleClickDetect = 0x00;
      }
      else
      {
        /* Disable TIM4 Capture Compare Channel 1/3 */
        TIM_CCxCmd(TIM4, TIM_Channel_1, DISABLE);
        TIM_CCxCmd(TIM4, TIM_Channel_3, DISABLE);
        
        /* Enable TIM4 Capture Compare Channel 2 */
        TIM_CCxCmd(TIM4, TIM_Channel_2, ENABLE);
        /* Sets the TIM4 Capture Compare2 Register value */
        TIM_SetCompare2(TIM4, TIM_CCR/12);

        /* Enable TIM4 Capture Compare Channel 4 */
        TIM_CCxCmd(TIM4, TIM_Channel_4, ENABLE);
        /* Sets the TIM4 Capture Compare4 Register value */
        TIM_SetCompare4(TIM4, TIM_CCR/12);
        
        /* Time base configuration */
        TIM_SetAutoreload(TIM4, TIM_ARR/12);
        Delay(200);   
        SingleClickDetect = 0;
      }
    }
  }
}
