#include "lis302dl_setup.h"
#include "main.h"

//Calibration offsets
__IO int8_t XOffset;
__IO int8_t YOffset;
__IO int8_t ZOffset;

/**
  * @brief  MEMS accelerometre management of the timeout situation.
  * @param  None.
  * @retval None.
  */
uint32_t LIS302DL_TIMEOUT_UserCallback(void)
{
  /* MEMS Accelerometer Timeout error occured */
  while (1);
}

/**
  * @brief  Waits for the user to press and release the "user" button.
  * @param  None.
  * @retval None.
  */
void wait_for_button_press(void)
{
	while (!STM_EVAL_PBGetState(BUTTON_USER))
		Delay(10); // This is 100 ms
	
	while (STM_EVAL_PBGetState(BUTTON_USER))
		Delay(10);
}
/**
  * @brief  Calculates the average acceleration.
  * @param  avgX pointer to average acceleration on the x axis.
  * @param  avgY pointer to average acceleration on the y axis.
  * @param  avgZ pointer to average acceleration on the z axis.
  * @retval None.
  */
void read_average_accel(float* avgX, float* avgY, float* avgZ)
{
	uint8_t Buffer[6];
	
	register float x, y, z;
	
    //Sum the values
	for (int i = 0; i < NUM_MEASUREMENTS; i++)
	{
        //Reads from the OUT_X, OUT_Y and OUT_Z registers
		LIS302DL_Read(Buffer, LIS302DL_OUT_X_ADDR, 6);
		
        //Sum acceleration on the respective axis.
        //Multiple buffer[i] with LIS302DL_SENSITIVITY_2_3G (18mg) to convert it to mg
		x += LIS302DL_SENSITIVITY_2_3G * (int8_t) Buffer[0]; //Buffer[0] access the x value
		y += LIS302DL_SENSITIVITY_2_3G * (int8_t) Buffer[2]; //Buffer[2] access the y value
		z += LIS302DL_SENSITIVITY_2_3G * (int8_t) Buffer[4]; //Buffer[4] access the z value
		
		Delay(10); //Wait 100 ms.
	}
	
    //Average the values.
	x /= NUM_MEASUREMENTS;
	y /= NUM_MEASUREMENTS;
	z /= NUM_MEASUREMENTS;
	
    //Calculate the magnitude which is the acceleration of gravity.
	float magnitude = sqrt(pow(x,2) + pow(y,2) + pow(z,2));
	
	printf("Acceleration: %f mg\n", magnitude);
	
    //Store the averages into the pointers
	if (avgX) *avgX = x;
	if (avgY) *avgY = y;
	if (avgZ) *avgZ = z;
}
/**
  * @brief  Calibrate LIS302DL.
  * @param  None
  * @retval None.
  */
void calibrate(void)
{
    float x1, y1, z1;
    float x2, y2, z2;
    
    printf("Calibrate the X axis\n");
	printf("Calibrate for +1g\n");
	wait_for_button_press();
	read_average_accel(&x1, NULL, NULL);
	printf("Calibrate for -1g\n");
	wait_for_button_press();
	read_average_accel(&x2, NULL, NULL);
    
    printf("Calibrate the Y axis\n");
	printf("Calibrate for +1g\n");
	wait_for_button_press();
	read_average_accel(NULL, &y1, NULL);
	printf("Calibrate for -1g\n");
	wait_for_button_press();
	read_average_accel(NULL, &y2, NULL);
    
    printf("Calibrate the Z axis\n");
	printf("Calibrate for +1g\n");
	wait_for_button_press();
	read_average_accel(NULL, NULL, &z1);
	printf("Calibrate for -1g\n");
	wait_for_button_press();
	read_average_accel(NULL, NULL, &z2);
	
    //Average the measurements so that the results are symmetric around 0g.
	XOffset = (x1 + x2) / 2;
    YOffset = (y1 + y2) / 2;
	ZOffset = (z1 + z2) / 2;
}

/**
  * @brief  Setup LIS302DL.
  * @param  None
  * @retval None.
  */
int setup_lis302dl()
{
	uint8_t ctrl = 0;
  
  LIS302DL_InitTypeDef  LIS302DL_InitStruct;
  LIS302DL_InterruptConfigTypeDef LIS302DL_InterruptStruct;  
  
  /* SysTick end of count event each 10ms */
  SysTick_Config(SystemCoreClock/ 100);
  
  /* Set configuration of LIS302DL*/
  LIS302DL_InitStruct.Power_Mode = LIS302DL_LOWPOWERMODE_ACTIVE;
  LIS302DL_InitStruct.Output_DataRate = LIS302DL_DATARATE_100;
  LIS302DL_InitStruct.Axes_Enable = LIS302DL_X_ENABLE | LIS302DL_Y_ENABLE | LIS302DL_Z_ENABLE;
  LIS302DL_InitStruct.Full_Scale = LIS302DL_FULLSCALE_2_3;
  LIS302DL_InitStruct.Self_Test = LIS302DL_SELFTEST_NORMAL;
  LIS302DL_Init(&LIS302DL_InitStruct);
    
  /* Set configuration of Internal High Pass Filter of LIS302DL*/
  LIS302DL_InterruptStruct.Latch_Request = LIS302DL_INTERRUPTREQUEST_LATCHED;
  LIS302DL_InterruptStruct.SingleClick_Axes = LIS302DL_CLICKINTERRUPT_Z_ENABLE;
  LIS302DL_InterruptStruct.DoubleClick_Axes = LIS302DL_DOUBLECLICKINTERRUPT_Z_ENABLE;
  LIS302DL_InterruptConfig(&LIS302DL_InterruptStruct);

  /* Required delay for the MEMS Accelerometre: Turn-on time = 3/Output data Rate 
                                                             = 3/100 = 30ms */
  Delay(30);
  
  /* Configure Interrupt control register: enable Click interrupt1 */
  ctrl = 0x07;
  LIS302DL_Write(&ctrl, LIS302DL_CTRL_REG3_ADDR, 1);
  
  /* Enable Interrupt generation on click/double click on Z axis */
  ctrl = 0x70;
  LIS302DL_Write(&ctrl, LIS302DL_CLICK_CFG_REG_ADDR, 1);
  
  /* Configure Click Threshold on X/Y axis (10 x 0.5g) */
  ctrl = 0xAA;
  LIS302DL_Write(&ctrl, LIS302DL_CLICK_THSY_X_REG_ADDR, 1);
  
  /* Configure Click Threshold on Z axis (10 x 0.5g) */
  ctrl = 0x0A;
  LIS302DL_Write(&ctrl, LIS302DL_CLICK_THSZ_REG_ADDR, 1);
  
  /* Configure Time Limit */
  ctrl = 0x03;
  LIS302DL_Write(&ctrl, LIS302DL_CLICK_TIMELIMIT_REG_ADDR, 1);
    
  /* Configure Latency */
  ctrl = 0x7F;
  LIS302DL_Write(&ctrl, LIS302DL_CLICK_LATENCY_REG_ADDR, 1);
  
  /* Configure Click Window */
  ctrl = 0x7F;
  LIS302DL_Write(&ctrl, LIS302DL_CLICK_WINDOW_REG_ADDR, 1);
	
	return 0;
}
