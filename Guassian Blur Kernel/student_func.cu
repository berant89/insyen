//Image blurring kernel

#include "utils.h"

#define BLKW 32 //blockDim.x
#define BLKH 16 //blockDim.y

uchar4* d_inputImageRGBA;
uchar4* d_outputImageRGBA__;
float* d_filter;

/**
 * @brief Performs a gaussian blur
 * @param inputImage A vector containing the RGB and alpha channels from the image.
 * @param outputImage A vector containing the RGB and alpha channels will contain the result.
 * @param numRows The number of rows in the image.
 * @param numCols The number of columns in the image.
 * @param filter The vector containing values for the gaussian blur
 * @param filterWidth Contains the width of the filter
 */
__global__
void gaussian_blur(const uchar4* const inputImage,
                   uchar4* const outputImage,
                   int numRows, int numCols,
                   const float* const filter, const int filterWidth)
{
  //Retrieve the 2D position if the thread from the grid
	const int2 thread_2D_pos = make_int2(blockIdx.x * BLKW + threadIdx.x, blockIdx.y * BLKH + threadIdx.y);
  //Convert into a 1D position for the array.
	const int thread_1D_pos = thread_2D_pos.y * numCols + thread_2D_pos.x;

  //make sure we don't try and access memory outside the image
  //by having any threads mapped there return early
	if(thread_2D_pos.x >= numCols || thread_2D_pos.y >= numRows)
		return;

	int rows = numRows - 1;
	int cols = numCols - 1;
	int halfWidth = filterWidth/2;
	float3 result;
	result.x = 0;
	result.y = 0;
	result.z = 0;

  //Perform loop for blur
  //Row loop
	for(int filter_r = -halfWidth; filter_r <= halfWidth; ++filter_r)
	{
    //Column loop
		for(int filter_c = -halfWidth; filter_c <= halfWidth; ++filter_c)
		{
      //Obtain the position of the thread
			int i = min(max(thread_2D_pos.y+ filter_r, 0), rows);
			int j = min(max(thread_2D_pos.x+ filter_c, 0), cols);
      //Calculate 1D position
			int idx = i*numCols + j;

      //Calculate the filter value
			float f_value = filter[(filter_r+halfWidth)*filterWidth + filter_c + halfWidth];
      
      //Extract the channels from the image
      float3 value;
      value.x = static_cast<float>(inputImage[idx].x);
      value.y = static_cast<float>(inputImage[idx].y);
      value.z = static_cast<float>(inputImage[idx].z);

      //Perform blur at the given pixel.
			result.x += value.x * f_value;
			result.y += value.y * f_value;
			result.z += value.z * f_value;
		}
	}

  //Store the results in the output image.
	outputImage[thread_1D_pos].x = static_cast<unsigned char>(result.x);
	outputImage[thread_1D_pos].y = static_cast<unsigned char>(result.y);
	outputImage[thread_1D_pos].z = static_cast<unsigned char>(result.z);
}

/**
 * @brief Allocates memory on the GPU and copies the host content.
 * @param h_inputImageRGBA Pointer to the input image from the host.
 * @param d_outputImageRGBA Pointer to the output image for the device.
 * @param numRowsImage The number of rows in the input image.
 * @param numColsImage The number of columns in the input image.
 * @param h_filter Pointer to the host filter for the blur.
 * @param filterWidth The width of the filter from the host.
 */
void allocateMemoryAndCopyToGPU(const uchar4* const h_inputImageRGBA,
                                uchar4* d_outputImageRGBA,
                                const size_t numRowsImage, const size_t numColsImage,
                                const float* const h_filter, const size_t filterWidth)
{
  const size_t numPixels = numRowsImage * numColsImage;
  //allocate memory on the device for both input and output
  checkCudaErrors(cudaMalloc(&d_inputImageRGBA, sizeof(uchar4) * numPixels));
  checkCudaErrors(cudaMalloc(&d_outputImageRGBA, sizeof(uchar4) * numPixels));
  checkCudaErrors(cudaMemset(d_outputImageRGBA, 0, numPixels * sizeof(uchar4))); //make sure no memory is left laying around
  d_outputImageRGBA__ = d_outputImageRGBA; //Copy the pointer for the cleanup function.

  //copy input array to the GPU
  checkCudaErrors(cudaMemcpy(d_inputImageRGBA, h_inputImageRGBA, sizeof(uchar4) * numPixels, cudaMemcpyHostToDevice));

  //Allocate the device filter
  checkCudaErrors(cudaMalloc(&d_filter, sizeof(float) * filterWidth * filterWidth));

  //Copy the host filter to the device
  checkCudaErrors(cudaMemcpy(d_filter, h_filter, sizeof(float) * filterWidth * filterWidth, cudaMemcpyHostToDevice));
}

/**
 * @brief Frees the memory that has been allocated on the device.
 */
void cleanup() 
{
  checkCudaErrors(cudaFree(d_inputImageRGBA));
  checkCudaErrors(cudaFree(d_outputImageRGBA__));
  checkCudaErrors(cudaFree(d_filter));
}

void execute_gaussian_blur(uchar4* const d_outputImageRGBA, 
                        const size_t numRows,
                        const size_t numCols,
                        const int filterWidth)
{
  //Set reasonable block size
	const dim3 blockSize(BLKW, BLKH, 1);

  //Compute correct grid size from the image size and and block size.
  const dim3 gridSize((numCols + BLKW - 1)/BLKW, (numRows + BLKH - 1)/BLKH, 1);
  
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
  gaussian_blur<<<gridSize, blockSize>>>(d_inputImageRGBA, d_outputImageRGBA, numRows, numCols, d_filter, filterWidth);
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

}