package question2;

public class Cholsky
{
	private static float[] e;
	private static float[][] Lt;
	
	/**
	 * Creates the lower triangular matrix.
	 * @param matrix to be decomposed.
	 * @return L the lower triangular matrix.
	 */
	public static float[][] choleskyFact(float[][] matrix)
	{
		int n = matrix.length;
		float[][] lower = new float[n][n];
		
		for(int j = 1; j <= n; j++)
		{
			float sum = (float) 0;
			for(int k = 1; k <= j-1; k++)
			{
				sum += (float) Math.pow(lower[j-1][k-1], 2);
			}
			
			float value = matrix[j-1][j-1] - sum;
			if(value < 0)
				throw new RuntimeException("Square root of a negative number.");
			else
				lower[j-1][j-1] = (float) Math.sqrt(value);
			
			for(int i = j+1; i <= n; i++)
			{
				sum = (float) 0;
				for(int k = 1; k <= j-1; k++)
				{
					sum += (float) lower[i-1][k-1] * lower[j-1][k-1];
				}
				
				if(lower[j-1][j-1] == 0)
					throw new RuntimeException("Divide by 0");
				else
					lower[i-1][j-1] = (float) (matrix[i-1][j-1] - sum)/lower[j-1][j-1];
			}
		}
		
		return lower;
	}
	
	/**
	 * Generates the appropriate b to for Ax=b, by doing the following: b=Ae, e=transpose([1,1,...,1]).
	 * @param A the hilbert matrix.
	 * @return b for Ax=b.
	 */
	public static float[] gen_b(float[][] A)
	{
		int n = A.length;
		e = new float[n];
		float[] b = new float[n];
		
		for(int i = 0; i < n; i++)
			e[i] = (float) 1;
		
		for(int i = 0; i < n; i++)
		{
			float sum = (float) 0;
			for(int j = 0; j < n; j++)
			{
				sum += A[i][j] * e[j];
			}
			b[i] = sum;
		}
		
		return b;
	}
	
	/**
	 * Solves Ax=b, by solving the following: Lz=b, transpose(L)x=z.
	 * @param matrix is the A.
	 * @param b is the solution.
	 * @return x the solution to the above equation.
	 */
	public static float[] solve(float[][] L, float[] b)
	{
		int n = L.length;
		float[] x = new float[n];
		float[] z = new float[n];
		
		//Forward substitution
		for(int i = 0; i < n; i++)
		{
			float sum = (float) 0;
			for(int j = 0; j < i; j++)
			{
				 sum += L[i][j] * z[j];
			}
			z[i] =(b[i] - sum)/L[i][i];
		}
		
		//Back substitution
		Lt = transpose(L);
		for(int i = n - 1; i >= 0;i--)
		{
			float sum = (float) 0;
			for(int j = i + 1; j < n; j++)
				sum += Lt[i][j] * x[j];
			x[i] = (z[i] - sum)/Lt[i][i];
		}
		
		return x;
	}
	
	/**
	 * Calculates the relative error.
	 * @param xc the computed solution
	 * @return the relative error.
	 */
	public static float relativeerr(float[] xc)
	{
		int n = xc.length;
		float[] diff = new float[n]; //Difference between xc and e.
		
		for(int i = 0; i < n; i++)
			diff[i] = xc[i] - e[i];
		return (float) norm2(diff)/norm2(xc);
	}
	
	/**
	 * Calculates the relative residual.
	 * @param A the matrix from Ax=b
	 * @param b the vector from Ax=b
	 * @param xc the vector from Ax=b, where x=xc
	 * @return the residual
	 */
	public static float residual(float[][] A, float[] b, float[] xc)
	{
		int n = xc.length;
		float[] product = new float[n];
		float[] diff = new float[n];
		product = matvecmult(A, xc);
		
		for(int i = 0;i < n; i++)
			diff[i] = b[i] - product[i];
		return norm2(diff)/(fnorm(A) * norm2(xc));
	}
	
	/**
	 * Calculates the matrix residual.
	 * @param A Matrix.
	 * @param L Matrix.
	 * @return the value of the matrix residual.
	 */
	public static float matrixresidual(float[][] A, float[][] L)
	{
		int n = A.length;
		float[][] prod = new float[n][n];
		prod = matrixmult(L, Lt);
		float[][] diff = new float[n][n];

		//Loop by row
		for(int i = 0; i < n; i++)
			//Loop by each column element.
			for(int j = 0; j < n; j++)
				diff[i][j] = A[i][j] - prod[i][j];
		return fnorm(diff)/fnorm(A);
	}
	
	/**
	 * Generates a Hilbert matrix of size nxn.
	 * @param n size of the matrix.
	 * @return A Hilbert matrix of size nxn.
	 */
	public static float[][] hilbert(int n)
	{
		float[][] hilb = new float[n][n];
		
		//Loop by row
		for(int i = 1; i <= n; i++)
			//Loop by each column element.
			for(int j = 1; j <= n; j++)
				hilb[i-1][j-1] = (float) (1.0/(i + j - 1));
		
		return hilb;
	}
	
	/**
	 *  Does the transpose of the matrix.
	 * @param L the lower triangular matrix, this can also be used for any other matrix.
	 * @return The transposed matrix returns.
	 */
	public static float[][] transpose(float[][] L)
	{
		int n = L.length;
		//Loop through each row
		for(int i = 0; i < n; i++)
			//Loop through each column element
			for(int j = i+1; j < n; j++)
			{
				float temp = L[i][j];
				L[i][j] = L[j][i];
				L[j][i]  = temp;
			}
		return L;
	}
	
	/**
	 * Calculates the 2-norm also the Euclidean norm.
	 * @param v is a vector.
	 * @return the value.
	 */
	public static float norm2(float[] v)
	{
		float sum = (float) 0;
		//Loop through each element
		for(int i = 0; i < v.length; i++)
		{
			sum += v[i]*v[i];
		}
		return (float) Math.sqrt(sum);
	}
	
	/**
	 * Calculates the F-norm of a given matrix.
	 * @param A is a matrix.
	 * @return the result of the F-norm.
	 */
	public static float fnorm(float[][] A)
	{
		int n = A.length;
		float[][] At = new float[n][n];
		At = transpose(A);
		float sum = (float) 0;
		//Loop through each vector in the matrix
		for(int i = 0; i < n; i++)
		{
			//Perform a norm2 on the given vector
			float val = norm2(At[i]);
			sum += (float) val * val;
		}
		return (float) Math.sqrt(sum);
	}
	
	/**
	 * Matrix vector multiplication.
	 * @param A the matrix.
	 * @param v the vector.
	 * @return a vector from the product of Av.
	 */
	public static float[] matvecmult(float[][] A, float[] v)
	{
		int n = v.length;
		float[] product = new float[n];
		//Loop through each row of the matrix
		for(int i = 0; i < n; i++)
		{
			float sum = (float) 0;
			//Loop through each column for the matrix and vector
			for(int j = 0; j < n; j++)
			{
				sum += A[i][j] * v[j];
			}
			product[i] = sum;
		}
		return product;
	}
	
	/**
	 * Matrix matrix multiplication.
	 * @param A Matrix.
	 * @param B Matrix.
	 * @return C matrix from AB = C.
	 */
	public static float[][] matrixmult(float[][] A, float[][] B)
	{
		int n = A.length;
		float[][] C = new float[n][n];
		for(int i = 0; i < n; i++)
			for(int j = 0; j < n; j++)
				for(int k = 0; k < n; k++)
					C[i][j] += A[i][k] * B[k][j];
		return C;
	}
	
	/**
	 * Prints a nxn matrix.
	 * @param mat is the square matrix.
	 */
	public static void print(float[][] mat)
	{
		int n = mat.length;
		//Loop through each row
		for(int i = 0; i < n; i++)
		{
			//Loop through each column
			for(int j = 0; j < n; j++)
				System.out.printf("%.7f  ", mat[i][j]);
		System.out.println();
		}
	}
	
	/**
	 * Prints out a vector.
	 * @param vec is a vector.
	 */
	public static void printv(float[] vec)
	{
		int n = vec.length;
		for(int i = 0; i < n; i++)
			System.out.printf("%.7f\n", vec[i]);
	}
	
	/**
	 * Does the computation prints out the results.
	 * @param hilb the hilbert matrix.
	 * @param L the lower triangular matrix.
	 * @param b the vector from Ax=b.
	 * @param x the vector from Ax=b.
	 * @param n the size of the matrix and vector(s).
	 */
	public static void output(float[][] hilb, float[][] L, float[] b, float[] x, int n)
	{
		hilb = hilbert(n);
		System.out.println("Hilbert matrix:");
		print(hilb);
		
		System.out.println();
		L = choleskyFact(hilb);
		System.out.println("Lower triangular matrix:");
		print(L);
		
		System.out.println();
		b = gen_b(hilb);
		System.out.println("b vector of Ax=b:");
		printv(b);
		
		System.out.println();
		x = solve(L, b);
		System.out.println("x the solution to Ax=b:");
		printv(x);
		
		System.out.println();
		
		System.out.println("The relative error: "+ relativeerr(x));
		System.out.println("Relative residual: "+ residual(hilb, b, x));
		System.out.println("The relative matrix "+ matrixresidual(hilb, L));
		
		System.out.println();
	}
	
	public static void main(String[] args)
	{
		for(int n = 2; n <= 8; n +=2)
		{
			System.out.println("Size of the hilbert matrix: " + n);
			//Instantiates the following for the hilbert matrix, the lower triangular matrix, the vector b and the vector x.
			float[][] hilb = new float[n][n];
			float[][] L = new float[n][n];
			float[] b = new float[n];
			float[] x = new float[n];
		
			output(hilb, L, b, x, n);
			System.out.println();
		}
	}
}
