<?php
use Parse\ParseObject;
use Parse\ParseUser;
use Parse\ParseQuery;
use Parse\ParseException;
/**
 * @brief Creates and stores a new user account on the system. If there's a 
 * 		  failure then the ouput is sent to the error flag in the html portion.
 * @param fNew_member An array containing the data to upload to database.
 * @return A boolean on the success of the function.
 */
function createAccount($fNew_member)
{
	try
	{
		$newUser = new ParseUser(); //Create new parse object
		$keys = array_keys($fNew_member); //Get all the keys
		// Loop through each key and set them into the object.
		foreach($keys as $key)
			$newUser->set($key, $fNew_member[$key]);
		$newUser->set('isTrucker', strcmp($newUser->get('Type'), 'Trucker') == 0);
		$newUser->signUp();
		$_SESSION['newUser'] = $newUser;
		
		return true;
	}
	catch(ParseException $ex)
	{
		$_SESSION['error'] = $ex->getMessage();
		return false;
	}
}

/**
 * @brief Creates and stores a new truck on the system. If there's a 
 *		  failure then the ouput is sent to the error flag in the html portion.
 *		  It also create a certificate object associated with this truck.
 * @param fUser The user associated with this truck.
 * @param fNew_truck An array containing the data to upload to database.
 * @return A boolean on the success of the function.
 */
function createTruck($fNew_truck, $fUser)
{
	try
	{
		$newTruck = new ParseObject("Truck"); //Create new truck
		if(strcmp($fNew_truck['hazmat'], "Hazmat_Y") == 0)
			$newTruck->set('hazmat', True);
		elseif(strcmp($fNew_truck['hazmat'], "Hazmat_N") == 0)
			$newTruck->set('hazmat', False);
		$newTruck->set('type', intval($fNew_truck['type']));
		$newTruck->set('registeredOriginLocation', $fNew_truck['registeredOriginLocation']);
		$newTruck->set('insurance', intval($fNew_truck['insurance']));
		$newTruck->set('notes', $fNew_truck['notes']);
		$newTruck->save();
		$fUser->set("currentTruck", $newTruck); // Insert the truck ID into the User associated with it
		$fUser->save();
		$newCertificate = new ParseObject("Certificate"); //Create new certificate
		$newCertificate->set('name', $fNew_truck['certificate']);
		$newCertificate->set("truck", $newTruck); // Insert the truck ID into the Certificate associated with it
		$newCertificate->save();
		return true;
	}
	catch(ParseException $ex)
	{
		$_SESSION['error'] = $ex->getMessage();
		return false;
	}
}

/**
 * @brief Updates the user with the given data specified by the given username.
 * @param fData An array containing the data to be updated to the database.
 * @param fUser the Parse object of the user
 * @return A boolean on the success of the function.
 */
function updateUser($fUser, $fData)
{
	try
	{
		/*$keys = array_keys($fData);
		$fUser->set('name', $fData['name']);
		$fUser->set('address', $fData['address']);
		$fUser->set('phone', $fData['phone']);
		$fUser->set('cellPhone', $fData['cellPhone']);
		$fUser->set('companyName', $fData['companyName']);
		$fUser->set('email', $fData['email']);
		// TODO : does Parse send email verification when eamil changes <---------------------------- ??????????????
		$fUser->save();*/
		return true;
	}
	catch(ParseException $ex)
	{
		echo $ex->getMessage();
		return false;
	}
}

/**
 * @brief Updates the truck with the given data specified by the given truck_id.
 * @param fData An array containing the data to be updated to the database.
 * @param fTruck the Parse object of the truck
 * @return A boolean on the success of the function.
 */
function updateTruck($fTruck, $fData)
{
	try
	{
		if(strcmp($fData['hazmat'], "Hazmat_Y") == 0)
			$fTruck->set('hazmat', True);
		elseif(strcmp($fData['hazmat'], "Hazmat_N") == 0)
			$fTruck->set('hazmat', False);
		$fTruck->set('type', intval($fData['type']));
		$fTruck->set('registeredOriginLocation', $fData['registeredOriginLocation']);
		$fTruck->set('insurance', intval($fData['insurance']));
		$fTruck->set('notes', $fData['notes']);
		$fTruck->save();
		// TODO : update the certificates <------------------------------------------------------ !!!!!!!!!!!!!!!!!
		return true;
	}
	catch(ParseException $ex)
	{
		echo $ex->getMessage();
		die();
		return false;
	}
}

/**
 * @brief checks to see if the given username is logged in.
 * @return A boolea on the success of the function.
 */
function loggedIn()
{
	if(array_key_exists("currentUser",$_SESSION))
		$currentUser = $_SESSION['currentUser'];
	else
		return false;
	if($currentUser !== null && isset($_SESSION['loggedin']) && isset($_SESSION['Username']))
		return true;
	return false;
}

/**
 * @brief Logsout the user.
 * @return A boolean on the success of the function.
 */
function logoutUser()
{
	ParseUser::logOut();
	unset($_SESSION['Username']);
	unset($_SESSION['loggedin']);

	return true;
}

/**
 * @brief Validates the given username and password before logging
 *		  the user into the system
 * @param fUsername The string of the given username
 * @param fPassword The string of the given password
 */
function validateUser($fUsername, $fPassword)
{
	try
	{
		$user = ParseUser::logIn($fUsername, $fPassword);
		$_SESSION['currentUser'] = $user;
		$_SESSION['Username'] = $fUsername;
		$_SESSION['loggedin'] = true;
		return true;
	}
	catch(ParseException $ex)
	{
		return false;
	}
}

/**
 * @brief Gets the type of the user specified by the given username
 * @param username The string of the given username
 */
function getTypeOfUser($username) 
{
	$query = ParseUser::query();
	$query->equalTo("username", $username);
	$results = $query->find();
	if(count($results) == 0)
		return null;
	return $results[0]->get('Type');
}

/**
 * @brief Gets the truck object of the user specified by the given username
 * @param username The string of the given username
 */
function getTruck($username)
{
	try {
		$query = ParseUser::query();
		$query->equalTo("username", $username);
		$results = $query->find();
		if(count($results) == 0)
			return null;
		$truck_query = new ParseQuery("Truck");
		$truck = $results[0]->get('currentTruck');
		return $truck_query->get($truck->getObjectId());
	} catch (ParseException $ex) {
		return null;
	}
}

/**
 * @brief Gets the certificate objects of the user specified by the given truck
 * @param truck The object of the given truck
 */
function getCertificates($truck)
{
	try {
		$query = new ParseQuery("Certificate");
		$query->equalTo("truck", $truck);
		$results = $query->find();
		if(count($results) == 0)
			return null;
		return $results;
	} catch (ParseException $ex) {
		return null;
	}
}

/**
 * @brief Sends an email to the user for changing password
 * @param email The string of the given email
 */
function resetPassword($email) 
{
	try 
	{
		ParseUser::requestPasswordReset($email); // Password reset request was sent successfully
		return true;
	}
	catch (ParseException $ex)
	{
		$_SESSION['error'] = $ex->getMessage();
		return false;
	}
}
?>